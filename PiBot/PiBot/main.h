/*
 * main.h
 *
 * Created: 6/10/2018 16:17:39
 *  Author: Tambet
 */ 


#ifndef MAIN_H_
#define MAIN_H_

#define F_CPU 16000000

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>


#include "Draiverid/ADC.h"
#include "Draiverid/Board.h"
#include "Draiverid/Encoders.h"
#include "Draiverid/Motors.h"
#include "Draiverid/SPI.h"
#include "Draiverid/usart.h"
#include "Draiverid/MPU9250.h"

uint8_t Commandor(uint8_t* command, uint32_t* ret_val);
uint8_t Sensor_commandor(uint8_t* command, uint32_t* ret_val);
uint32_t Spi_adc_read(uint32_t channel);
uint8_t Spi_imu_read(void);

#endif /* MAIN_H_ */