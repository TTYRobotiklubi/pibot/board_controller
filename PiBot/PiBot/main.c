/*
 * PiBot.c
 *
 * Created: 28.05.2018 23:42:32
 * Author : Tambet
 */ 

#include "main.h"

uint16_t servo1_val = 0;
uint16_t servo2_val = 0;

int32_t enc1_val = 0;
int32_t enc2_val = 0;

int main(void)
{
	ADC_Init();
	Spi_master_init();
	Usart_Init();
		
	int val = 0;
	uint16_t bat = 0;
	uint8_t ADC_conf=0;
	char buff[100];
	unsigned short ic_ADC1[8], ic_ADC2[8];
	int16_t ic_IMU[8];
	
    while(1) 
    {
		
		if(Usart_available())
		{
			//ADC conf
			if(ADC_conf == 3)
			{
				PORTF |= (1<<5) | (1<<7);
			}
			else{
				if(ADC_conf&1)
				{
					PORTF |= (1<<5);
					PORTF &= ~(1<<7);
				}
				else if(ADC_conf&2)
				{
					PORTF &= ~(1<<5);
					PORTF |= (1<<7);
				}
				else
				{
					PORTF &= ~((1<<5) | (1<<7));
				}
			}
			
			// Usart Get
			Usart_geta(buff, 6);
			Usart_puts(buff);

			val =atoi(buff+2);
			switch(buff[0])
			{
				case 'a':
					if(val==3)
					{
						Read_first_sensors(ic_ADC1);
						Read_second_sensors(ic_ADC2);	
						for (int i = 0, l=0; i < 16; i++)
						{
							if(i < 8)
								l += sprintf(&buff[l],"%04d,",ic_ADC1[i]);
							else
								l += sprintf(&buff[l],"%04d,",ic_ADC2[i-8]);
						}
					}
					else
					{
						if(val&1)
						{
							Read_first_sensors(ic_ADC1);
							for (int i = 0, l=0; i < 8; i++)
							{
								l += sprintf(&buff[l],"%04d,",ic_ADC1[i]);
							}
						}
						else if(val&2)
						{
							Read_second_sensors(ic_ADC2);
							for (int i = 0, l=0; i < 8; i++)
							{
								l += sprintf(&buff[l],"%04d,",ic_ADC2[i]);
							}
						}
					}
					break;
					
				case 'b':
					sprintf(buff, "%04u", bat);
					break;
					
				case 'c':
					Motors_speed_right(val);
					sprintf(buff, "motorR");
					break;
					
				case 'd':
					Motors_speed_left(val);
					sprintf(buff, "motorL");
					break;
					
				case 'l':
					Motors_speed_right(val);
					Motors_speed_left(val);
					sprintf(buff, "motorB");
					break;
					
				case 'e':
					OCR0A=val;
					sprintf(buff, "servo1");
					break;
					
				case 'f':
					OCR0B=val;
					sprintf(buff, "servo2");
					break;
					
				case 'g':
					Buzzer_set(val%255);
					sprintf(buff, "buzzer");
					break;
					
				case 'h':
					sprintf(buff, "%011ld:%011ld", enc1_val, enc2_val);
					break;
					
				case 'i':
					MPU9250_Read_data(ic_IMU);
					for (int i = 0, l=0; i < 7; i++)
					{
						l+=sprintf(&buff[l],"%07d,",ic_IMU[i]);
					}
					break;
					
				case 'j':
					_delay_ms(100);
					initMPU9250(buff);
					break;
					
				case 'm':
					Motors_init();
					sprintf(buff, "motorE");
					break;
					
				case 's':
					Servo_init();
					sprintf(buff, "servoE");
					break;
					
				case 't':
					Encoders_init();
					sprintf(buff, "EncE");
					break;
					
					
				case 'x':
					ADC_conf = val;
					sprintf(buff, "%03u", ADC_conf);
					break;
				
				default:
					sprintf(buff, "b:%04d",val);
					break;
			}
			PORTF &= ~(1<<5);
			Spi_cs_off();
			//Servo_set();

			Usart_puts(buff);
		}
		bat = Read_battery();
		if(bat < 751)
		{
			PORTD &= ~(1<<7);
		}

	}
}


uint8_t Spi_imu_read(void)
{
	DDRF |= (1<<7);
	PORTF |= (1<<7);
	
	unsigned char buff[30];
	PORTD  |= (ADC_CC1 | ADC_CC2);
	PORTB &= ~IMU_CC;
	Spi_transmit(0x55/*imu message*/);
	Spi_receive_array(buff, 30);
	
	PORTF &= ~(1<<7);
	
	return 0x00;
}
