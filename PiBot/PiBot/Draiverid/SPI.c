/*
 * SPI.c
 *
 * Created: 11.06.2018 18:12:56
 *  Author: Lenovo
 */ 
#include "SPI.h"

//SPI initialize for SD card
void Spi_prep_adc1(void)
{
	PORTB |= IMU_CC;
	PORTD &= ~ADC_CC1;
	PORTD |= ADC_CC2;
}

void Spi_prep_adc2(void)
{
	PORTB |= IMU_CC;
	PORTD |= ADC_CC1;
	PORTD &= ~ADC_CC2;
}

void Spi_prep_imu(void)
{
	PORTB &= ~IMU_CC;
	PORTD |= ADC_CC1;
	PORTD |= ADC_CC2;
}

void Spi_cs_off(void)
{
	PORTB |= IMU_CC;
	PORTD |= ADC_CC1 | ADC_CC2 ;
}

void Spi_master_init(void)
{
	// Set MOSI, SCK, SS outputs
	DDR_SPI |= (1<<DD_MOSI) | (1<<DD_SCK);
	
	// Set SD Chip select as output
	DDRB |=  1<<DDB0;			// IMU
	DDRD |=  1<<DDD4|1<<DDD6;	// ADC1 | ADC2
	
	Spi_cs_off();
	
	// Set inputs
	DDR_SPI &= ~(1<<DD_MISO);
	
	
	// Enable SPI, Master
	SPCR = (1<<SPE) | (1<<MSTR) | (1<<CPOL)| (1<<CPHA)  ;
	
	// data order MSB first, clock polarity Rising_Falling
	SPCR &= ~(1<<DORD) | (1<<CPOL) | (1<<CPHA) ;
	
	// set clock rate prescale f/16
	SPCR |= (1<<SPR0);
	SPSR = 0;
}


void Spi_transmit(unsigned char data)
{
	// Start transmission
	SPDR = data;

	// Wait for transmission complete
	while(!(SPSR & (1<<SPIF)));
}

unsigned char Spi_receive(void)
{
	unsigned char data;
	// Wait for reception complete
	while(!(SPSR & (1<<SPIF)));
	data = SPDR;

	// Return data register
	return data;
}

void Spi_transmit_array(unsigned char* data, uint8_t len)
{
	uint8_t i = 0;
	while(len > i)
	{
		Spi_transmit(data[i]);
		i++;	
	}
}

void Spi_receive_array(unsigned char* data, uint8_t len)
{
	uint8_t i = 0;
	while(len > i)
	{
		data[i] = Spi_receive();
		i++;
	}
}

void Read_first_sensors(unsigned short *ic_ADC1)
{
	unsigned char lsb;
	unsigned char msb;
	
	_delay_us(300);
	
	for (int i = 0; i < 8; i++)
	{
		Spi_prep_adc1(); // set CS signal for ADC1 chip low
		Spi_transmit(1);
		Spi_transmit(0x80 | (i << 4));
		
		msb = Spi_receive();
		Spi_transmit(0xff);
		lsb = Spi_receive();
		
		ic_ADC1[i] = ((msb & 0x03) << 8) | lsb;
		PORTD |= (1<<4); // release ADC1 CS
	}
	
}

void Read_second_sensors(unsigned short *ic_ADC2)
{
	unsigned char lsb;
	unsigned char msb;
	
	for (int i = 0; i < 8; i++)
	{
		Spi_prep_adc2(); // set CS signal for ADC2 chip low
		Spi_transmit(1);
		Spi_transmit(0x80 | (i << 4));
		
		msb = Spi_receive();
		Spi_transmit(0xff);
		lsb = Spi_receive();
		
		ic_ADC2[i] = ((msb & 0x03) << 8) | lsb;
		PORTD |= (1<<6); // release ADC2 CS
	}
}