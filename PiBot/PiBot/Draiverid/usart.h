/*
 * usart.h
 *
 * Created: 28.05.2014 20:53:34
 *  Author: Robotiklubi
 */ 


#ifndef USART_H_
#define USART_H_

#include "../main.h"


#define RXD		PORTD2
#define TXD		PORTD3

#define USART_BAUDRATE 115200
#define BAUD_PRESCALE 8
// Initialize USART
// baud:	9600		- 103
//		19200	- 51
//		38400	- 25
//		57600	- 16
//		115200	- 8
void Usart_Init(void);

// Send string
void Usart_puts(char* StringPtr);

// Send array
// data to be sent, data lenght
void Usart_puta(char * dataPtr, uint8_t len);

// Send char
void Usart_putc( unsigned char data );

// check if available
uint8_t Usart_available(void);

// Receive char
unsigned char Usart_getc( void);

void Usart_gets(char *dest);

void Usart_geta(char *dest, uint8_t len);


#endif /* USART_H_ */