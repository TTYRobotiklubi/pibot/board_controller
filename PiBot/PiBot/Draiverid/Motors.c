/*
 * motors.c
 *
 * Created: 28.09.2014 17:53:38
 *  Author: Robotiklubi
 */ 
#include "motors.h"

void Motors_init()
{
	DDRD |= (1<<7);//Enable motors
	PORTD |= (1<<7);
	
	DDRB |= (1<<4)|(1<<5)|(1<<6);//set outputs
	DDRC |= (1<<6);

	TCCR1A = (1<<WGM11)|(2<<COM1B0)|(2<<COM1A0);//fast pwm
	ICR1 = 100;	//resolution
	TCCR1B = (1<<WGM13)|(1<<WGM12)|(5<<CS10);//cs10 - prescaler
	Motors_speed_right(0);
	Motors_speed_left (0);
}

void Motors_speed_right(int8_t right)
{
	if (right ==  0)
	{
		PORTC |= (1<<6);
		OCR1B = 100;
	}
	else if (right > 0)
	{
		PORTC |= (1<<6);
		OCR1B = 100 - right;
	}
	else
	{
		PORTC &= ~(1<<6);
		OCR1B = -right;
	}
	
}

void Motors_speed_left(int8_t left)
{
	if (left ==  0)
	{
		PORTB |= (1<<4);
		OCR1A = 100;
	}
	else if (left > 0)
	{
		PORTB |= (1<<4);
		OCR1A = 100 - left;
	}
	else
	{
		PORTB &= ~(1<<4);
		OCR1A = -left;
	}
}

void Buzzer_set(uint32_t val)
{
	DDRC |= (1<<7);
	
	TCCR4A = (1<<WGM11)|(2<<COM1B0)|(2<<COM1A0);//fast pwm
	TCCR4B = (1<<WGM13)|(1<<WGM12)|(3<<CS10);//cs10 - prescaler 8
	
	OCR4A = val%255;
}

void Servo_init(void)
{
	// Enable servos
	DDRF |= (1<<6);
	PORTF |= (1<<6);
	
	// Enable servo outputs
	DDRB |= (1<<7);
	DDRD |= (1<<0);
	
	TCCR0A = (1<<COM0A1)|(1<<COM0B1)|(3<<WGM00);
	TCCR0B = (1<<CS02 | 1<<CS00);
	
	OCR0A = 34;
	OCR0B = 34;
	 
}

void Servo_set(void)
{

	/*if(servo1_val < SERVO_1_MIN){servo1_val = SERVO_1_MIN;}
	else if(servo1_val < SERVO_1_MAX){servo1_val = SERVO_1_MAX;}
		
	if(servo2_val < SERVO_2_MIN){servo2_val = SERVO_2_MIN;}
	else if(servo2_val < SERVO_2_MAX){servo2_val = SERVO_2_MAX;}*/
		
	OCR0A = servo1_val;
	OCR0B = servo2_val;
}