/*
 * usart.c
 *
 * Created: 28.05.2014 20:50:49
 *  Author: Robotiklubi
 */ 

#include "usart.h"

void Usart_Init(void)
{
	UBRR1 = BAUD_PRESCALE;					
	UCSR1B = (1<<RXEN1)|(1<<TXEN1);		//TX and RX enabled
	UCSR1C = 0b00000110;		//8-bits
	UCSR1D = 0;
	
	PORTD |= (1<<3);
	DDRD |= (1<<3);
}


void Usart_puts(char* StringPtr)
{ uint32_t i = 0;
	while(StringPtr[i] != '\0')
	{
		while(!(UCSR1A & (1<<UDRE1)));
		UDR1 = StringPtr[i];
		i++;
	}
}

void Usart_puta(char * dataPtr, uint8_t len)
{
	for (uint8_t i = 0; i < len; i++)
	{
		while(!(UCSR1A & (1<<UDRE1)));
		UDR1 = dataPtr[i];
	}
}


void Usart_putc( unsigned char data )
{
	// Wait for empty transmit buffer
	while( !( UCSR1A &(1<<UDRE1)) );
	// Put data into buffer, sends the data
	UDR1 = data;
}

uint8_t Usart_available(void)
{
	return (UCSR1A & (1<<RXC1));
}

unsigned char Usart_getc( void)
{
	//  Wait for data to be received
	while( !(UCSR1A & (1<<RXC1)) );
	// Get and return received data from buffer
	return UDR1;
}

void Usart_gets(char *dest)
{	
	uint8_t i = 0;
	while(UCSR1A & (1<<RXC1))
	{
		dest[i] = UDR1;
		i++;
	}
}

void Usart_geta(char *dest, uint8_t len)
{	uint8_t i = 0;
	while(len > i)
	{
		while( !(UCSR1A & (1<<RXC1)) );
		
		dest[i] = UDR1;
		i++;
	}
	dest[i+1]='\0';
}