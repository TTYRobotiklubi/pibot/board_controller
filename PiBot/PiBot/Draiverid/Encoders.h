/*
 * Encoders.h
 *
 * Created: 13.06.2018 13:31:49
 *  Author: Lenovo
 */ 


#ifndef ENCODERS_H_
#define ENCODERS_H_

#include "../main.h"


#define ENC1_A (1<<6)
#define ENC1_B (1<<0)
#define ENC2_A (1<<1)
#define ENC2_B (1<<5)

void Encoders_init(void);




#endif /* ENCODERS_H_ */