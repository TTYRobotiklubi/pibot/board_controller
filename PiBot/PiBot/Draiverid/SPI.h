/*
 * SPI.h
 *
 * Created: 11.06.2018 18:09:24
 *  Author: Lenovo
 */ 


#ifndef SPI_H_
#define SPI_H_

#include "../main.h"

#define DDR_SPI		DDRB
#define DD_MOSI		DDB2
#define DD_MISO		DDB3
#define DD_SCK		DDB1

#define IMU_CC (1<<0)
#define ADC_CC1 (1<<4)
#define ADC_CC2 (1<<6)


void Spi_prep_adc1(void);
void Spi_prep_adc2(void);
void Spi_prep_imu(void);
void Spi_cs_off(void);
void Spi_master_init(void);
void Spi_transmit(unsigned char data);
unsigned char Spi_receive(void);
void Spi_transmit_array(unsigned char* data, uint8_t len);
void Spi_receive_array(unsigned char* data, uint8_t len);
void Read_first_sensors(unsigned short *ic_ADC1);
void Read_second_sensors(unsigned short *ic_ADC2);



#endif /* SPI_H_ */