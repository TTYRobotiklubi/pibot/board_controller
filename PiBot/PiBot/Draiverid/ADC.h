/*
 * adc.h
 *
 * Created: 28.09.2014 18:10:52
 *  Author: Robotiklubi
 */ 


#ifndef ADC_H_
#define ADC_H_

#include "../main.h"

#define ADC_MAX_CHANNEL 10

extern volatile unsigned int channels[10];
extern const uint8_t selected_channels[11];
extern const uint8_t numberOfChannels;

void ADC_Init();
uint16_t ADC_read(uint8_t channel);
uint16_t Read_battery(void);

#endif /* ADC_H_ */