

#include "MPU9250.h"




uint8_t SPI_read_MPU9250_register( uint8_t reg )
{
	PORTB &= ~(1<<0);
	
	Spi_transmit(reg | 0x80); // reg | 0x80 to denote read
	Spi_transmit(0xff); // write 8-bits 1
	uint8_t read_value = Spi_receive(); 
	
	PORTB |= (1<<0);
	return read_value;
}

/************************************************************************/

void SPI_write_MPU9250_register( uint8_t reg, uint8_t value )
{
	PORTB &= ~(1<<0);
	Spi_transmit(reg); 
	Spi_transmit( value );
	PORTB |= (1<<0);
}

void initMPU9250(char* buff)
{	
	if (  SPI_read_MPU9250_register(MPU9250_WHO_AM_I) == 0x71)	
	{
		sprintf(buff, "IMUOK");
		
		SPI_write_MPU9250_register(MPU9250_PWR_MGMT_1,0x80); // Reset Device 
		_delay_ms(200);
		SPI_write_MPU9250_register(MPU9250_PWR_MGMT_1,0x01); // Clock Source
		SPI_write_MPU9250_register(MPU9250_PWR_MGMT_2,0x00); // Enable Acc & Gyro
		
		SPI_write_MPU9250_register(MPU9250_CONFIG,0x02); // 92Hz digital filter for gyroscope
		
		SPI_write_MPU9250_register(MPU9250_GYRO_CONFIG,0b00011011); // Fchoice 11 | GYRO 2000 dps
		
		SPI_write_MPU9250_register(MPU9250_ACCEL_CONFIG,0b00011000); //Accel Full Scale Select: 16g
		
		SPI_write_MPU9250_register(MPU9250_ACCEL_CONFIG2,0x02 | (1 << 3)); // 99Hz accelerometer DLPF
	}
	else
	{
		sprintf(buff, "IMUNO");
	}

}

void MPU9250_Read_data(int16_t *sensors)
{
	
	PORTB &= ~(1<<0); // set CS signal for MPU chip low
	
	unsigned char lsb;
	unsigned char msb;
	
	Spi_transmit(MPU9250_ACCEL_XOUT_H | 0x80); // reg | 0x80 to denote read
	
	for (int i = 0; i < 7; i++)
	{
		
		Spi_transmit(0xff);
		msb = Spi_receive();
		
		Spi_transmit(0xff);
		lsb = Spi_receive();
		
		sensors[i] = (msb << 8) | lsb;
	}
	
	PORTB |= (1<<0); // release CS signal for MPU chip low
}

