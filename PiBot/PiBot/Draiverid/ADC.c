#include "ADC.h"

void ADC_Init()
{
	ADMUX = (1<<REFS0);							// AVcc as reference
	ADCSRA = (1<<ADEN)|(7<<ADPS0);				//125kHz, Enabled
	ADCSRB = (1<<ADHSM);						//High speed mode
}


uint16_t ADC_read(uint8_t channel)
{
	if(channel <= 7)
	{
		ADCSRB &= ~(1<<MUX5);
		ADMUX = (1<<REFS0)|channel;	// Select channel
		
		ADCSRA |= (1<<ADSC);		// Start conversion
		while(ADCSRA & (1<<ADSC));	// Wait for conversion complete
		return ADC;
	}
	else
	{
		ADCSRB |= (1<<MUX5);
		ADMUX = (1<<REFS0)|(channel-8);	// Select channel
		
		ADCSRA |= (1<<ADSC);		// Start conversion
		while(ADCSRA & (1<<ADSC));	// Wait for conversion complete
		return ADC;
	}
}

uint16_t Read_battery(void)
{
	return ADC_read(4);
}
