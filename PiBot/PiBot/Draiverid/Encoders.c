/*
 * Encoders.c
 *
 * Created: 13.06.2018 13:31:30
 *  Author: Lenovo
 */ 
#include "Encoders.h"
#include<avr/interrupt.h>

extern int32_t enc1_val;
extern int32_t enc2_val;

void Encoders_init(void)
{
	//inputs
	DDRE &= ~ENC1_A;
	DDRF &= ~ENC1_B;
	DDRD &= ~(ENC2_A | ENC2_B);
	
	sei();
	EICRA |= (3<<ISC10);
	EICRB |= (3<<ISC60);
	
	EIMSK |= (1<<INT6 | 1<<INT1);
	
	enc1_val = 0;
 	enc2_val = 0;
}


ISR(INT6_vect)
{
	if(PINF & (1<<0))
	{
		enc1_val++;	
	}
	else
	{
		enc1_val--;
	}
}

ISR(INT1_vect)
{
	if(PIND & (1<<5))
	{
		enc2_val--;
	}
	else
	{
		enc2_val++;
	}
}