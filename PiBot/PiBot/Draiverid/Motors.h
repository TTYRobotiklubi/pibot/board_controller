/*
 * motors.h
 *
 * Created: 28.09.2014 17:53:29
 *  Author: Robotiklubi
 */ 


#ifndef MOTORS_H_
#define MOTORS_H_

#include "../main.h"

#define SERVO_1_MIN 25
#define SERVO_1_MAX 36
#define SERVO_2_MIN 30
#define SERVO_2_MAX 42

extern uint16_t servo1_val;
extern uint16_t servo2_val;

void Motors_init();
void Motors_speed_right(int8_t right);
void Motors_speed_left(int8_t left);
void Buzzer_set(uint32_t val);
void Servo_init(void);
void Servo_set(void);

#endif /* MOTORS_H_ */